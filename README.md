# check_sip.py

usage: check_sip.py [-h] -u sip:user@voip.example.com -H HOST -p PORT [-t 60] [-d]

Check SIP endpoint

### optional arguments:
    -h, --help              show this help message and exit
    -t 60, --timeout 60     Connection timeout (default: 60s)
    -d, --debug             Enable debug output (default: no)
    -p PROTOCOL, --protocol PROTOCOL Choose the protocol (default: udp)

### required arguments:
    -u sip:user@voip.example.com, --uri sip:user@voip.example.com The uri to the endpoint
    -H HOST, --host HOST  The IP of the host
    -p PORT, --port PORT  The port of the endpoint
