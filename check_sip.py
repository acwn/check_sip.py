#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import random
import socket
import sys
from string import Template
import re

class IcingaMsg:
	def __init__ (self, message, level):
		m_level = ["OK", "WARNING", "CRITICAL", "UNKNOWN"]
		print ("%s  %s" % (m_level[level], message))
		sys.exit (level)

class Application:
	def __init__ (self, uri, host, port, timeout, protocol, debug):
		self.m_uri = uri
		self.m_host = host
		self.m_port = int (port)
		self.m_timeout = timeout
		self.m_protocol = protocol
		self.m_debug = debug
		self.m_version = "0.3"

	def GenerateTag (self):
		l_chars = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9']
		l_moresecure = random.SystemRandom ()
		l_tag = ''
		for x in range (8):
			l_tag = l_tag + (l_moresecure.choice (l_chars))
		return (l_tag)

	def GenerateInvite (self, localhost, localport):
		d = dict (d_localhost=localhost, d_localport=localport, d_dsturi= self.m_uri, d_version=self.m_version, d_tag=self.GenerateTag (), d_idtag = self.GenerateTag ())
		l_invite = Template ("OPTIONS $d_dsturi SIP/2.0\n"
		"Via: SIP/2.0/UDP $d_localhost:$d_localport;rport\n"
		"From: sip:checksip.py@$d_localhost:$d_localport;tag=$d_tag\n"
		"To: $d_dsturi\n"
		"Call-ID: $d_idtag@$d_localhost\n"
		"CSeq: 1 OPTIONS\n"
		"Contact: sip:checksip@$d_localhost:$d_localport\n"
		"Content-length: 0\n"
		"Max-Forwards: 70\n"
		"User-agent: check_sip.py $d_version\n"
		"Accept: text/plain\n")
		if self.m_debug:
			print (l_invite.substitute (d))
		return (l_invite.substitute (d))

	def Connect (self):
		try:
			if self.m_protocol == "udp":
				s = socket.socket (socket.AF_INET, socket.SOCK_DGRAM)
			elif self.m_protocol == "tcp":
				s = socket.socket (socket.AF_INET, socket.SOCK_STREAM)
			else:
				raise ValueError ('Unknown protocol. Use udp or tcp.')
			s.settimeout (self.m_timeout)
			s.connect ((self.m_host, self.m_port))
			s.send (self.GenerateInvite (self.m_host, self.m_port).encode())
			r = s.recv (4096)
			if self.m_debug:
				print (r)
			self.Verify (r.decode ())
		except ConnectionError as e:
			IcingaMsg (e, 3)
		except OSError as e:
			IcingaMsg (e, 2)
		except ValueError as e:
		  IcingaMsg (e, 3)

	def Verify (self, answer):
		expect = re.search ("^SIP/2\.0 200.*", answer)
		failure = re.search ("^SIP/2\.0 [3-6][0-9][0-9].*", answer)
		if expect:
			IcingaMsg (expect.group(0), 0)
		if failure:
			IcingaMsg (failure.group(0), 2)


def main():
	parser = argparse.ArgumentParser (description='Check SIP endpoint')
	requiredargs = parser.add_argument_group ('required arguments')

	requiredargs.add_argument ("-u", "--uri", help="The uri to the endpoint", metavar="sip:user@voip.example.com", required=True)
	requiredargs.add_argument ("-H", "--host", help="The IP of the host", required=True)
	requiredargs.add_argument ("-P", "--port", help="The port of the endpoint", required=True)
	parser.add_argument ("-t", "--timeout", default="60", type=int, help="Connection timeout (default: 60s)", metavar="60")
	parser.add_argument ("-d", "--debug", action='store_true', help="Enable debug output (default: no)")
	parser.add_argument ("-p", "--protocol", default="udp", help="Choose the protocol (default: udp)")
	args = parser.parse_args ()
	app = Application (args.uri, args.host, args.port, args.timeout, args.protocol, args.debug)
	app.Connect()

if __name__ == "__main__":
	main()
